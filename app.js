/* global dc,d3,Vue,VuePivot,crossfilter */

const data = [
    {year: 2019, age: 10, gender: 'M', trips: 17},
    {year: 2019, age: 15, gender: 'M', trips: 18},
    {year: 2019, age: 20, gender: 'F', trips: 19},
    {year: 2019, age: 25, gender: 'M', trips: 21},
    {year: 2019, age: 40, gender: 'M', trips: 34},
    {year: 2019, age: 35, gender: 'F', trips: 24},
    {year: 2018, age: 20, gender: 'M', trips: 24},
    {year: 2018, age: 25, gender: 'M', trips: 20},
    {year: 2018, age: 20, gender: 'F', trips: 17},
    {year: 2018, age: 25, gender: 'M', trips: 26},
    {year: 2018, age: 50, gender: 'F', trips: 40},
    {year: 2018, age: 45, gender: 'F', trips: 35},
    {year: 2017, age: 10, gender: 'M', trips: 12},
    {year: 2017, age: 25, gender: 'M', trips: 20},
    {year: 2017, age: 40, gender: 'F', trips: 23},
    {year: 2017, age: 45, gender: 'M', trips: 31},
    {year: 2017, age: 60, gender: 'M', trips: 40},
    {year: 2017, age: 55, gender: 'F', trips: 40},
    {year: 2017, age: 50, gender: 'F', trips: 34},
    {year: 2016, age: 20, gender: 'M', trips: 40},
    {year: 2016, age: 20, gender: 'F', trips: 40}
];

function rowChartResponses(ndx, element){
    const dim = ndx.dimension(row => row.gender);
    const grp = dim.group().reduceCount(row => row);
    const chart = dc.rowChart(element)
        .dimension(dim)
        .group(grp)
        .x(d3.scaleLinear())
        .elasticX(true)
        .ordering(group => group.key)
    ;
    chart.xAxis().ticks(4).tickFormat(d3.format('d'));
    chart.ordinalColors(['red', 'green']);
    return chart;
}

Vue.component('dc-pivot', {
    props: ['pivotDim'],
    data: function(){
        return {
            rows: this.pivotDim.top(Infinity),
            fields: [{
                label: 'age',
                headerSlotName: 'header-button',
                getter: item => item.age

            } ],
            rowFields: [{
                getter: item => item.year,
                headerSlotName: 'header-button',
                label: 'year'
            }],
            colFields: [{
                getter: item => item.gender,
                headerSlotName: 'header-button',
                label: 'gender'
            }],
            defaultShowSettings: true,
            reducer: (sum, item) => sum + item.trips
        };
    },
    template: '#dc-pivot',
    methods: {
        render: function(){
        },
        redraw: function(){
            this.rows = this.pivotDim.top(Infinity);
        },
        filterAll: function(){
        }
    },

    mounted: function(){
        dc.chartRegistry.register(this);
    },

    beforeDestroy: function(){
        dc.ChartRegistry.deregister(this);
    }
});


function main(){
    Vue.use(VuePivot);
    const ndx = crossfilter(data);
    const pivotDim = ndx.dimension(row => row);
    window.app = new Vue({
        el: '#app',
        data: function(){
            return {
                pivotDim: Object.freeze(pivotDim)
            };
        }
    });
    rowChartResponses(ndx, '#cat-chart').render();
}

function ready(fn) {
    if (document.readyState !== 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

ready(main);

